﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Notepad
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        OpenFileDialog openFileDialog = new OpenFileDialog
        {
            Title = "eyb0s",
            Filter = "Text file (*.txt)|*.txt| All Files (*.*)|*.*",
        };

        SaveFileDialog saveFileDialog = new SaveFileDialog
        {
            Title = "b0s pls",
            Filter = "Text file (*.txt)|*.txt",
        };

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            if (!(eyb0s.Text == string.Empty))
            {
               var result = MessageBox.Show("Are you sure you want to exit?", "ya got text kek", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);

               if (result == MessageBoxResult.Yes)
               {
                   Application.Current.Shutdown();
               }
            }

            else
                Application.Current.Shutdown();
        }

        private void New_Click(object sender, RoutedEventArgs e)
        {
            eyb0s.Text = "";
        }

        private void Open_Click(object sender, RoutedEventArgs e)
        {
            if (openFileDialog.ShowDialog() == true)
            {
                MainWindow window2 = new MainWindow();
                window2.eyb0s.Text = File.ReadAllText(openFileDialog.FileName);
                window2.Show();
            }
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            if (saveFileDialog.ShowDialog() == true)
                File.WriteAllText(saveFileDialog.FileName, eyb0s.Text);
        }

        private void About_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("ayyy lmao this is a notepad b0s");
        }
    }
}
